$(document).ready(function(){
  console.log("Welcome to examples");

  // INICIO EJEMPLO 1
  var contador = 1;
  // INICIO FUNCIÓN AÑADIR
  $("#add").on("click", function() {
    // Crear una capa
    let bubble = $("<div />").addClass("bubble");
    // Añadir el contador y sumarle 1
    bubble.text(contador++);
    // Insertar la capa en el ejemplo 1 dentro de container
      $("#e1 > .container").append(bubble);
  });
  // FIN FUNCIÓN AÑADIR

  // INICIO FUNCIÓN ELIMINAR PRIMERO
  $("#delete-first").on("click", function() {
      $("#e1 > .container > .bubble").first().remove();
  });
  // FIN FUNCIÓN ELIMINAR PRIMERO

  // INICIO FUNCIÓN ELIMINAR PRIMERO
  $("#delete-last").on("click", function() {
      $("#e1 > .container > .bubble").last().remove();
  });
  // FIN FUNCIÓN ELIMINAR PRIMERO
  // FIN EJEMPLO 1

  // INICIO EJEMPLO 2
  // INICIO FUNCIÓN DROP (ARRASTRA)

//  $(".draggable").on("dragover", function() {  alert("dropeado");});

    /*  $(".paper").draggable();
        $(".trash").droppable({
            drop: function(event, ui) {
                $(this).css('background', 'rgb(0,200,0)');
            },
            over: function(event, ui) {
                $(this).css('background', 'orange');
            },
            out: function(event, ui) {
                $(this).css('background', 'cyan');
            }
        }); */

  $('.draggable').draggable({
    start: function () {
        $(this).animate({
            opacity: '0.5'
        }, 1000);
    },
    stop: function () {
        $(this).animate({
            opacity: '1'
        }, 1000);
    }
  });

  $('.droppable').droppable({
      drop: function () {
          $('#draggable').animate({
              opacity: 0.5
          }, 1000);//.remove();
          $(this).html('Dropped');
      }
  });

  // FIN EJEMPLO 2

  // INICIO EJEMPLO 3
  $("#clone").on("click", function() {
    let nelements = $('#nelements').val();
    // elemento
    let elemento = $("#e3 .bubble").first();
    let contenedor = $("#e3 .container");

    for (let i = 0; i < nelements; i++) {
      contenedor.append(elemento.clone());
    }

  });
  // FIN EJEMPLO 3

  // INICIO EJEMPLO 4
  $("#slide").on("click", function() {
    $("#e4 .bubble").fadeToggle();
  });
  // FIN EJEMPLO 4

  // INICIO EJEMPLO 5
  $("#show-content").on("click", function() {
    // Borrar el contenido
    $("#e5 .result").find("div").each(function() {
      $(this).remove();
    });

    $("#e5 .bubble").each(function() {
      $("#e5 .result").append($("<div />").text($(this).text()));
    });

  });

  $("#add-word").on("click", function() {
    let newText = $("#new-word").val();

    if (newText != "") { // Si el nuevo texto es diferente de "" (texto vacío)
      // Añadimos una bubble
      let bubble = $("<div />").addClass("bubble");
      bubble.text($("#new-word").val());
      $("#e5 .container").append(bubble);

      // Mensaje correcto
      $("#e5 .log").empty()
                   .append($("<div />").text("El texto ha sido introducido correctamente."))
                   .css({"color" : "green"})
                   .fadeOut(4000, function() { $(this).empty().fadeIn(); });

    } else { // Mensaje de error, el texto no puede ser vacío
      $("#e5 .log").empty()
                   .append($("<div />").text("El texto debe contener almenos un carácter."))
                   .css({"color" : "red"})
                   .fadeOut(4000, function() { $(this).empty().fadeIn(); });

    }
  });
  // FIN EJEMPLO 5

  // INICIO EJEMPLO 6
  $("#show-content").on("click", function() {

  });

  $(".drag").draggable({
     cursor: 'move',
     revert: true,
     helper: 'clone'
  });

  $(".drop").droppable({
  	accept: '.drag',
    drop: function(event, ui) {
              console.log(ui.draggable.prop("id") + " dropped onto " + $(this).prop("id"));
            }

   });

  // FIN EJEMPLO 6



});
