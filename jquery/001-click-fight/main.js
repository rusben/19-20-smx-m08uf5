$(document).ready(function(){
  console.log("Welcome to jQuery.");
});

/* Javascript */

var dayArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function getTime() {
  var today = new Date(),
      h = today.getHours(),
      m = today.getMinutes().toString().padStart(2, '0'),
      s = today.getSeconds().toString().padStart(2, '0'),
      d = dayArray[today.getDay()],
      da = today.getDate(),
      mo = monthArray[today.getMonth()],
      y = today.getFullYear();



  document.getElementById('time').innerHTML =
  `<h1 class='large'>${h}:${m}:${s}</h1>&nbsp;` +

  `<span class='dark'>${d},</span>&nbsp;` +
  `<span class='dark'>${da}</span>&nbsp;` +
  `<span class='dark'>${mo}</span>&nbsp;` +
  `<span class='dark'>${y}</span>`;

};

setInterval (function() {
  getTime();
}, 1000);
