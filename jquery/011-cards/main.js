$(document).ready(function(){
  console.log("Welcome to cards");

  $( "#tablero" ).sortable();
  $( "#tablero" ).disableSelection();

  crearBaraja();

  function crearBaraja() {

    var cards = [];

    cards.push(crearPicas());
    cards.push(crearRombos());
    cards.push(crearTreboles());
    cards.push(crearCorazones());

    for ( var i = 0; i < cards.length; i++ ) {
        $("#tablero").append(cards[i]);
    }

  }

  function crearPalo(palo) {

    var cards = [];

    // Crear del 1 al 10
    for (var numero = 1; numero <= 10; numero++) {
      cards.push(crearCarta(palo, numero));
    }

    // Crear J, Q, K
    cards.push(crearCarta(palo, "J"));
    cards.push(crearCarta(palo, "Q"));
    cards.push(crearCarta(palo, "K"));

    return cards;

  }

  function crearPicas() {
    return crearPalo("picas");
  }

  function crearRombos() {
    return crearPalo("rombos");
  }

  function crearTreboles() {
    return crearPalo("treboles");
  }

  function crearCorazones() {
    return crearPalo("corazones");
  }

  function crearCarta(palo, valor) {

    let card = $("<div />").addClass("carta");

    // Añade la carta con la clase indicada en el palo
    // y con el valor indicado
    switch (palo) {
      case "rombos": card.addClass("rombos").text(valor);
        break;
      case "picas": card.addClass("picas").text(valor);
        break;
      case "treboles": card.addClass("treboles").text(valor);
        break;
      case "corazones": card.addClass("corazones").text(valor);
        break;
      default:
        break;
    }

    return card;

  }


});
