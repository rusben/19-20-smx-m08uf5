$(document).ready(function(){
  console.log("Welcome to Tic Tac Toe Pro Masters.");

  // Creamos una variable para decidir el turno
  var turno = "player1"; // posibles valores, player1 (X) o player2 (O)

  // Añadimos 9 celdas a my-canvas
  for (var i = 0; i < 9; i++) {
    $("#tictactoe").append('<div class="cell"> </div>');
  }
  // Capturamos los eventos
  // Evento click
  $(".cell").on("click", function() {
    // Si el texto que tiene la celda clickada en el momento del click
    // es igual a un espacio, entonces podemos poner la ficha
    if ($(this).text() == " ") {
      // Si el turno es player1 introduciremos una X
      if (turno == "player1") {
        $(this).text("X");
        // Actualizamos el valor del turno
        turno = "player2";
      } else { // sabemos que será player2
        // Si el turno es player2 introduciremos una O
        $(this).text("O");
        // Actualizamos el valor del turno
        turno = "player1";
      }
    }
  });




});
