$(document).ready(function(){
  console.log("input, checkbox, select examples");


  /* INICIO INPUT */
  $("#muestra-texto").on("click", function() {
    alert($("#texto").val());
  });

  $("#comprueba-texto").on("click", function() {
    var texto = $("#texto").val();

    if (texto.toLowerCase() == "hola") {
      alert("SI");
    } else {
      alert("NO");
    }
  });
  /* FIN INPUT */

  /* INICIO NUMERO */
  $("#comprueba-numero").on("click", function() {
    var numero = $("#numero").val();

    if (numero >= 5) {
      alert("SIIIIU");
    } else {
      alert("NO");
    }

  });

  // $("#numero").on("change", function() {
  //  alert("HOLAKEASE: "+ $("#numero").val());
  // });
  /* FIN NUMERO */


  /* INICIO SELECT */
  /* Capturar cuando cambia el estado del select */
  $("#cars").on("change", function(){
      // var selectedCar = $(this).children("option:selected").val();
      var selectedCar = $(this).val();
      alert("You have selected the car - " + selectedCar);


  });

  $("#comprueba-select").on("click", function() {
    var selectedCar = $("#cars").val();
    alert(selectedCar);
  });

  $("#add-logo").on("click", function() {
    var imagePath = $("#cars").children("option:selected").attr("img-href");
    alert("This is the path - " + imagePath);

    //$("#logos").append("<img src='"+imagePath+"' />");
    $("#logos").html("<img src='"+imagePath+"' />");
  });
  /* FIN SELECT */

  /* INICIO CHECKBOX */


  $("#comprueba-checkbox").on("click", function() {

    $("#vehicles").children().each(function() {
      if ($(this).is(':checked')) alert($(this).val());
    });

    // if ($("#vehicle1").is(':checked')) alert($("#vehicle1").val());
  });

  /* FIN CHECKBOX */

  /* INCIO RADIO BUTTONS */

  $("input[type='button']").change(function(){
    var radioValue = $("input[name='gender']:checked").val();
    if(radioValue){
        alert("Your are a - " + radioValue);
    }
  });

  $("#comprueba-radio").on("click", function() {
    var radioValue = $("input[name='gender']:checked").val();
    if(radioValue){
        alert("Your are a - " + radioValue);
    }
  });


/* FIN RADIO BUTTONS */



});
