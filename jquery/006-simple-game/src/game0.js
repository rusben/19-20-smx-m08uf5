/**
 * "game" this is the game core
 *
 * @name: game
 * @version: 0.0.1
 * @author: SMX-M08
 * @description: game core
 * @params {none}
 * @return: none
 */
var game = {
  version: "0.0.1",
  time: 0,
  playing: false,
  paused: false,
  elapsed: 0,
  version: function () {
    return this.version;
  },
  /**
   * @name: start
   * @description: Starts the game
   * @params {none}
   * @return: true if everything works fine
   */
  start: function () {
    if (this.paused) {
      this.paused = false;
      this.playing = true;
      this.time = new Date - this.elapsed;
    } else if (this.playing) {
      // Do nothing
    } else {
      this.time = new Date;
      this.playing = true;
    }
    return true;
  },
  /**
   * @name: pause
   * @description: Pauses the game
   * @params {none}
   * @return: true if everything works fine
   */
  pause: function () {
    if (this.playing) {
      this.elapsed = new Date - this.time;
      this.playing = false;
      this.paused = true;
      //console.log(this.timer());
    }

    return true;
  },
  /**
   * @name: reset
   * @description: Resets the game
   * @params {none}
   * @return: true if everything works fine
   */
  reset: function () {
      this.elapsed = 0;
      this.playing = false;
      this.paused = false;
      return true;
  },
  /**
   * @name: timer
   * @description: Returns the game timer
   * @params {none}
   * @return: the game timer
   */
  timer: function () {
    if (!this.playing && !this.paused) return 0;
    else if (this.paused) return this.elapsed;
    else return ((new Date - this.time));
  }
}
