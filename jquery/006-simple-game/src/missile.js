// https://en.wikipedia.org/wiki/Projectile_motion

class Missile {

  constructor(color, width, height, x, y) {
      this.color = color;
      this.width = width;
      this.height = height;
      this.x = $("tankA").x;
      this.y = $("tankA").y;
      $('<div />').appendTo(playground).addClass("missile");
  }

  draw(game, playground) {
    $(".missile").css("background-color",  this.color);
    $(".missile").css("width",  this.height);
    $(".missile").css("height",  this.width);

    var V0 = 40;
    var alpha = 50;
    var g = 9.81;
    var t = game.timer.seconds() / 1000;

    this.x = V0 * t * Math.cos(alpha);
    this.y = -((V0 * t * Math.sin(alpha)) - (0.5 * g * Math.pow(t, 2)));

    // tiempo de vuelo
    // t = (2 * V0 * Math.sin(alpha)) / g;

    console.log("x: "+this.x);
    console.log("y: "+this.y);
    console.log($(".missile"));

    $(".missile").css("top",  this.x);
    $(".missile").css("left",  this.y);
  }

}
