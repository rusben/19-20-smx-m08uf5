class Ball extends Component {

  constructor(parent, x, y, radius) {
      // Chain constructor with super
      super("ball", parent, x, y);
      // Add a new property
      this.radius = radius;
      this.id;
      this.css();
  }

  css() {
      this.element.css({"background-color": "red"});
      this.element.css({"height": this.radius});
      this.element.css({"width": this.radius});
      this.element.css({"border-radius": "50%"});
      this.element.css({"position": "relative"});

      // Show bounding box
      this.bb();
  }

  redraw(x, y) {
    //this.x = Math.random()*100;
    //this.y = Math.random()*100;
    this.x = x;
    this.y = y;

    this.draw();
  }

//https://stackoverflow.com/questions/19459449/running-requestanimationframe-from-within-a-new-object
  move() {
    this.redraw(this.x + 1, this.y + 1);
    //this.redraw(this.x,this.y);

    console.log("x: "+this.x+" y: "+this.y);
    this.id = requestAnimationFrame(this.move.bind(this));
  }

  stop() {
   cancelAnimationFrame(this.id);
  }


}
