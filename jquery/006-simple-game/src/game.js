/**
 * "game" this is the game core
 *
 * @name: game
 * @version: 0.0.1
 * @author: SMX-M08
 * @description: game core
 * @params {none}
 * @return: none
 */
class Game {
  constructor(playground) {
      this.version = "0.0.1";
      this.timer = new Timer();
      this.tankA = new Tank(1, "A", "red", "asteroid.png", 100, 100, 300, 0);
      this.tankB = new Tank(2, "B", "blue", "hola.png", 150, 150, 300, 300);
      this.playground = playground;
  }

  version() {
    return this.version;
  }

  /**
   * @name: start
   * @description: Starts the game
   * @params {none}
   * @return: true if everything works fine
   */
  start() {
    this.timer.start();
    this.tankA.draw(this.playground);
    this.tankB.draw(this.playground);
  }
  /**
   * @name: pause
   * @description: Pauses the game
   * @params {none}
   * @return: true if everything works fine
   */
  pause() {
    this.timer.pause();
  }
  /**
   * @name: reset
   * @description: Resets the game
   * @params {none}
   * @return: true if everything works fine
   */
  reset() {
    this.timer.reset();
    this.tankA.remove();
    this.tankB.remove();
    //this.playground.empty();
  }
  /**
   * @name: timer
   * @description: Returns the game timer
   * @params {none}
   * @return: the game timer
   */
  time() {
    return this.timer.seconds();
  }
}
