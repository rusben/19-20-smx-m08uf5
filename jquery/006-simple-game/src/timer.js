// Initializing a class
class Timer {
    constructor() {
        this.paused =  false;
        this.elapsed =  0;
        this.time = 0;
    }

    // Adding a method to the constructor
    greet() {
        return `${this.time} seconds`;
    }

    start() {
      if (this.paused) {
        this.paused = false;
        this.running = true;
        this.time = new Date - this.elapsed;
      } else if (this.running) {
        // Do nothing
      } else {
        this.time = new Date;
        this.running = true;
      }
    }

    reset() {
      this.elapsed = 0;
      this.running = false;
      this.paused = false;
    }

    pause() {
      if (this.running) {
        this.elapsed = new Date - this.time;
        this.running = false;
        this.paused = true;
      }
    }

    seconds() {
      if (!this.running && !this.paused) return 0;
      else if (this.paused) return this.elapsed;
      else return ((new Date - this.time));
    }
}

// Creating a new class from the parent
/*class Mage extends Hero {
    constructor(name, level, spell) {
        // Chain constructor with super
        super(name, level);

        // Add a new property
        this.spell = spell;
    }
}*/
