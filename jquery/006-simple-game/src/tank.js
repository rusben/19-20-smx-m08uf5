class Tank {
  constructor(player, name, color, image, width, height, x, y) {
      this.player = player;
      this.name = name;
      this.color = color;
      this.image = image;
      this.width = width;
      this.height = height;
      this.x = x;
      this.y = y;
      this.lives = 3;
      this.ammo = 1000;
      this.armor = 100;
  }

  shoot(playground) {

  }

  print() {
    console.log("color: "+this.color);
    console.log("image: "+this.image);
    console.log("width: "+this.width);
    console.log("height: "+this.height);
    console.log("x: "+this.x);
    console.log("y: "+this.y);
    console.log("lives: "+this.lives);
    console.log("ammo: "+this.ammo);
    console.log("armor: "+this.armor);
  }

  draw(playground) {
    $('<div />').appendTo(playground).addClass("tank").addClass("tank"+this.name);

    $(".tank"+this.name).css("background-color", this.color);
    $(".tank"+this.name).css("width", this.width);
    $(".tank"+this.name).css("height", this.height);

    this.x = $(window).height() - this.height;
    $(".tank"+this.name).css("top",  this.x);

    if (this.player == 1) {
      this.y = 0;
    } else {
      this.y = $(window).width() - this.width;
    }
    
    $(".tank"+this.name).css("left", this.y);

  }

  remove() {

  }

  suma(a, b) {
    return a + b;
  }

}
