class Component {

  constructor(myClass, parent, x, y) {
    this.myClass = myClass;
    this.parent = parent;
    this.x = x;
    this.y = y;
    this.element = this.init();
  }

  init() {
    return $('<div />').appendTo(this.parent).addClass(this.myClass);
  }

  draw() {
    this.element.css({top: this.x, left: this.y, position: 'absolute'});
    //$("#mydiv").parent().css({position: 'relative'});
    //$("#mydiv").css({top: 200, left: 200, position:'absolute'});
  }

  redraw(x, y) {
    this.draw();
  }

  // Bounding Box
  bb() {
    //$(this.element).wrap($('<div />').addClass("bb"));
  }

}
