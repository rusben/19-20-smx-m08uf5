$(document).ready(function(){
  console.log("Welcome to jQuery.");
/*
  var game = new Game($("#playground"));
  var missile = new Missile("yellow", 50, 50, 0, 0);
  var globalID;
  var timerID;
  var redrawID;

  function addAsteroid() {
    $('<div />').addClass("asteroid").appendTo("body");
    globalID = requestAnimationFrame(addAsteroid);
  }

  function redraw() {
    game.tankA.draw();
    game.tankB.draw();

    missile.draw(game, playground);
    //game.tankA.shoot(game.playground);

    redrawID = requestAnimationFrame(redraw);
  }

  function runTimer() {
    $('.timer').text(game.time() / 1000);
    timerID = requestAnimationFrame(runTimer);
  }

  $("#start").on("click", function() {
//    globalID = requestAnimationFrame(addAsteroid);
    timerID = requestAnimationFrame(runTimer);
    redrawID = requestAnimationFrame(redraw);
    game.start();
  });

  $("#stop").on("click", function() {
  //  cancelAnimationFrame(globalID);
    cancelAnimationFrame(timerID);
    cancelAnimationFrame(redrawID);
    game.pause();
  });

  $("#reset").on("click", function() {
  //  cancelAnimationFrame(globalID);
  //  $(".asteroid").remove();

    game.reset();
    cancelAnimationFrame(timerID);
    $('.timer').text(game.time() / 1000);

  });

  $("#shoot").on("click", function() {

  });
 */


var ball = new Ball($("#playground"), 25, 25, 100);

var timer = (function () {
    var instance;

    function createInstance() {
        var object = new Timer();
        return object;
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();


$("#start").on("click", function() {
  ball.move(timer);
  timer.getInstance().start();
});

$("#stop").on("click", function() {
  ball.stop();
  console.log($("#playground"));
  console.log($("#playground").position().top);
  console.log($("#playground").position().left);
  console.log($("#playground").width());
  console.log($("#playground").height());


  timer.getInstance().stop();

});

  console.log($("#playground"));
  console.log($("#playground").position().top);
  console.log($("#playground").position().left);
  console.log($("#playground").width());
  console.log($("#playground").height());

});
