$(document).ready(function(){

  // Añadimos my-canvas
  $("body").append('<div id="my-canvas"></div>');

  // Añadimos 100 celdas a my-canvas
  for (var i = 0; i < 10000; i++) {
    $("#my-canvas").append('<div class="cell"></div>');
  }

  $("#colorpicker").change(function(){
    console.log($("#colorpicker").val());
  });

  $(".cell").mousedown(function(){
      $(this).css("background-color", $("#colorpicker").val());
  });

  $("#reset").click(function(){
    $(".cell").css("background-color", "");
  });


});
